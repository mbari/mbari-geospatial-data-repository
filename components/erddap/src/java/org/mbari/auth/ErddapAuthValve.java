package org.mbari.auth;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Logger;

/**
 * This class implements the invoke method of the Tomcat ValveBase class so
 * that it can look for incoming OpenID Connect headers and translate them
 * to the appropriate Session attribute that the ERDDAP application is looking
 * for in order to identify the authenticated user.
*/
public class ErddapAuthValve extends ValveBase {

    // Create a logger
    private final static Logger LOGGER = Logger.getLogger(ErddapAuthValve.class.getName());

    /**
     * This is the implementation of the invoke method that will be called on
     * each incoming HTTP request.  It looks for a specific OpenID Connect
     * header value and then attaches that to a session attribute if one does
     * not exist.  This attribute is the one that ERDDAP looks for to identify
     * and authenticated user.
     * @param request is the incoming HTTP request
     * @param response is the outgoing response object
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void invoke(Request request, Response response) throws IOException, ServletException {
        LOGGER.fine("Received request");

        // Try to grab the session
        HttpSession session = request.getSession(true);
        if (session != null) {
            LOGGER.fine("Session exists and has ID: " + session.getId());

            // Look for the OIDC header which contains the email
            String oidcClaimEmail = request.getHeader("oidc_claim_email");

            if (oidcClaimEmail != null) {
                LOGGER.fine("Found existing OIDC Claim Email " + oidcClaimEmail + " and will attach to the session");

                // Attach it to the session
                session.setAttribute("loggedInAs:erddap", oidcClaimEmail);

            } else {
                LOGGER.fine("There was a session, but no OIDC email was found, strange ...");
            }
        } else {
            LOGGER.warning("Session came back null even after trying to create one, this should not happen");
        }

        // Now send the request on
        getNext().invoke(request, response);
    }
}
