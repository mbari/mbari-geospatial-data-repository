package org.mbari.resources.api;

public class UserAuthInfo {
    private String uuid;
    private String[] claims;
    private String[] scopes;

    public UserAuthInfo(String uuid, String[] claims, String[] scopes) {

    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String[] getClaims() {
        return claims;
    }

    public void setClaims(String[] claims) {
        this.claims = claims;
    }

    public String[] getScopes() {
        return scopes;
    }

    public void setScopes(String[] scopes) {
        this.scopes = scopes;
    }
}
