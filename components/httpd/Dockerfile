# This docker file builds an apache server using mod_auth_openidc as an authentication module.
# Start with CentOS 7
FROM centos:7

# Install all the necessary packages and build the mod_auth_openidc module
RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install \
    unzip \
    git \
    curl-devel \
    httpd \
    httpd-devel \
    wget \
    php \
    gcc* \
    make \
    libtool \
    openssl \
    openssl-devel \
    pcre-devel \
    mod_ssl \
    yum clean all && \
    cd /root && \
    wget https://github.com/akheron/jansson/archive/master.zip && \
    unzip master.zip && \
    cd jansson-master && \
    autoreconf -i && \
    ./configure && \
    make && \
    make install && \
    cd /root && \
    wget http://download.redis.io/releases/redis-4.0.2.tar.gz && \
    tar xzf redis-4.0.2.tar.gz && \
    cd redis-4.0.2 && \
    make && \
    make install && \
    cd /root && \
    git clone https://github.com/redis/hiredis.git && \
    cd hiredis && \
    make && \
    make install && \
    cd /root && \
    git clone https://github.com/cisco/cjose.git && \
    cd cjose && \
    autoreconf -f -i && \
    ./configure && \
    make && \
    make install && \
    cd /root && \
    git clone https://github.com/pingidentity/mod_auth_openidc.git && \
    cd mod_auth_openidc && \
    ./autogen.sh && \
    ./configure HIREDIS_CFLAGS="-I/usr/local/include/hiredis -D_FILE_OFFSET_BITS=64" HIREDIS_LIBS="-L/usr/local/lib" JANSSON_CFLAGS="-I/usr/local/include" JANSSON_LIBS="-I/usr/local/lib -ljansson" CJOSE_CFLAGS="-I/usr/local/include/cjose" CJOSE_LIBS="-L/usr/local/lib -lcjose" --with-apxs2=/usr/bin/apxs --with-hiredis && \
    make && \
    make install

# Add the configuration to load the modules necessary into the Apache server
ADD 14-auth-openidc.conf /etc/httpd/conf.modules.d/14-auth-openidc.conf

# Expose the HTTP port outside the container
EXPOSE 443

# Simple startup script to avoid some issues observed with container restart
ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]
