# This is a Docker build file that starts with a CentOS 7 image and builds another
# image that provides a service that monitors a file directory and generates
# file change events that are published to a RabbitMQ server.
FROM centos:7

# Now run the commands to build up the image (all run as root)
RUN yum upgrade -y && \
    yum update -y && \
    yum install -y curl \
    git \
    unzip \
    autoconf \
    automake \
    libtool \
    maven \
    gcc \
    gcc-c++ \
    make \
    java-1.8.0-openjdk-devel && \
    yum clean all && \
    cd /root && \
    git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    git checkout 3.6.x && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    ldconfig && \
    cd .. && \
    curl -L https://github.com/emcrisostomo/fswatch/releases/download/1.12.0/fswatch-1.12.0.tar.gz -O && \
    gunzip fswatch-1.12.0.tar.gz && \
    tar -xvf fswatch-1.12.0.tar && \
    cd fswatch-1.12.0 && \
    ./configure && \
    make && \
    make install && \
    # Now let's create the user that will run the fswatch software
    groupadd fswatch && \
    useradd -g fswatch -m -s /bin/bash fswatch && \
    echo 'fswatchpass' | passwd fswatch --stdin

# Switch to the fswatch user
USER fswatch

# Set the working directory
WORKDIR /home/fswatch

# Grab the file indexing project and build it
RUN cd /home/fswatch && \
   git clone https://bitbucket.org/mbari/data-tag-modeling.git && \
   cd data-tag-modeling && \
   mvn clean package install && \
   cd .. && \
   git clone https://bitbucket.org/mbari/file-to-es-indexer.git && \
   cd file-to-es-indexer && \
   mvn clean compile assembly:single

CMD "/home/fswatch/file-to-es-indexer/src/main/shell/fswatch-to-amqp-start.sh"
