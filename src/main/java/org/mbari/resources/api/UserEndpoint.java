package org.mbari.resources.api;

import com.google.gson.Gson;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/resources")
public class ResourceEndpoint {

    // Create a logger
    private static Logger logger = Logger.getLogger("ResourceEndpoint");

    // This is a boolean to indicate if the JWTs should be validated when received
    private boolean apiValidateTokens = false;

    // This is a string that is the name of the elasticsearch index (with a default value)
    private String indexName = "resources";

    // A GSON object to help with JSON processing
    private Gson gson = new Gson();

    @Inject
    DataStore dataStore;

    @Inject
    TokenUtilities tokenUtilities;

    @Context
    HttpHeaders httpHeaders;

    @PostConstruct
    private void init() {
        logger.info("Starting resource endpoint");

        // Grab the environment variable that specifies if the API should validate Bearer tokens on each call
        String apiValidateTokensString = System.getenv("API_VALIDATE_TOKENS");

        // If it was specified, convert it to a boolean and save it
        if (apiValidateTokensString != null) {
            try {
                apiValidateTokens = Boolean.getBoolean(apiValidateTokensString);
            } catch (Exception e) {
                logger.warning("Exception caught trying to convert API_VALIDATE_TOKENS of " +
                        apiValidateTokensString + " to a boolean: " + e.getMessage());
            }
        }

        // Now grab the name of the elasticsearch index from the environment
        String indexNameFromEnv = System.getenv("INDEX_NAME");
        if (indexNameFromEnv != null && !indexNameFromEnv.equals("")) {
            this.indexName = indexNameFromEnv;
        }

        // Read in the resource elasticsearch mapping file from the classpath
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(
                    getClass().getClassLoader().getResourceAsStream(
                            "es-resources-mapping.json")));
            String line;
            while ((line = br.readLine()) != null) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        // Grab the index configuration string
        String indexConfig = sb.toString();


        // Make sure the resource index has been created
        dataStore.createIndexWithMapping(this.indexName, indexConfig);

    }

    @GET
    @Produces(APPLICATION_JSON)
    public Response getResources(
            @QueryParam("content") List<String> content,
            @QueryParam("startCreationTimestamp") long startCreationTimestamp,
            @QueryParam("endCreationTimestamp") long endCreationTimestamp,
            @QueryParam("description") List<String> description,
            @QueryParam("extension") List<String> extension,
            @QueryParam("isDirectory") boolean isDirectory,
            @QueryParam("isFile") boolean isFile,
            @QueryParam("locator") List<String> locator,
            @QueryParam("mimeType") List<String> mimeType,
            @QueryParam("startModificationTimestamp") long startModificationTimestamp,
            @QueryParam("endModificationTimestamp") long endModificationTimestamp,
            @QueryParam("name") List<String> name,
            @QueryParam("resourceType") List<String> resourceType,
            @QueryParam("startSize") long startSize,
            @QueryParam("endSize") long endSize,
            @QueryParam("startTemporalCoverageTimestamp") long startTemporalCoverageTimestamp,
            @QueryParam("endTemporalCoverageTimestamp") long endTemporalCoverageTimestamp,
            @QueryParam("offset") int offset,
            @QueryParam("limit") int limit,
            @QueryParam("orderBy") List<String> orderBy) {

        logger.fine("getResources called");

        // First, use the utility methods to extract the Bearer token from the header (if there is one)
        String bearerTokenString = EndpointUtils.extractBearerTokenStringFromHTTPHeaders(httpHeaders);

        // If the API is supposed to validate the token, do so
        if (apiValidateTokens) {
            // TODO kgomes: implement this
        }

        // Now check to see if the access token contains the read permissions for this API
        boolean canRead = tokenUtilities.checkForMatchingClaim(bearerTokenString, "permission",
                "read") || tokenUtilities.checkForMatchingClaim(bearerTokenString, "permission",
                "write");

        // If there is a string, check to see if it's a JWT
        String jsonResources = "[]";
        if (canRead) {
            jsonResources = dataStore.getResources(this.indexName, content, startCreationTimestamp,
                    endCreationTimestamp, description,
                    extension, isDirectory, isFile, locator, mimeType, startModificationTimestamp, endModificationTimestamp,
                    name, resourceType, startSize, endSize, startTemporalCoverageTimestamp, endTemporalCoverageTimestamp,
                    offset, limit, orderBy);
        }

        // Build response and send
        return Response.ok(jsonResources).build();
    }

    @POST
    @Consumes(APPLICATION_JSON)
    public Response createResource(LinkedHashMap resourceHashMap, @Context UriInfo uriInfo) {

        logger.fine("createResource called");

        // First, use the utility methods to extract the Bearer token from the header (if there is one)
        String bearerTokenString = EndpointUtils.extractBearerTokenStringFromHTTPHeaders(httpHeaders);

        // Grab the subject from the token
        String callingSubject = tokenUtilities.getSubjectWithISSFromJWT(bearerTokenString);

        // Let's see if there is an owners array in the hashmap
        if (resourceHashMap.containsKey("owners")) {
            // Since it exists, grab the array list
            ArrayList<String> owners = (ArrayList<String>) resourceHashMap.get("owners");

            // Loop over them and look for the current calling user in the list of owners
            boolean ownerListed = false;
            for (String owner : owners) {
                if (owner.equals(callingSubject)) ownerListed = true;
            }

            // If not there, add it
            if (!ownerListed) {
                ((ArrayList<String>) resourceHashMap.get("owners")).add(callingSubject);
            }

        } else {
            // Create an array for the owners array
            String[] owners = {callingSubject};

            // Create and add the calling subject as the owner
            resourceHashMap.put("owners", owners);
        }

        // If the API is supposed to validate the token, do so
        if (apiValidateTokens) {
            // TODO kgomes: implement this
        }

        // Now check to see if the access token contains the write permissions for this API
        boolean canWrite = tokenUtilities.checkForMatchingClaim(bearerTokenString, "permission",
                "write");

        // If the caller has write privs, create the new Resource
        if (canWrite) {

            // A flag to indicate if an equivalent Resource is already in the data store
            boolean resourceExists = false;

            // This is the UUID of the incoming object (if available)
            UUID incomingUUID = null;

            // Now we need to make sure it does not exist already.  A Resource basically has a couple of primary keys.
            // You can use the ID of course, but we are over-riding the default autogen and using UUIDs for the IDs.
            // Also, the locators should be unique as well.  So, let first check to see if the incoming Resource already
            // has a UUID and if so, make sure the data store does not have one with the same UUID.
            if (resourceHashMap.containsKey("UUID") || resourceHashMap.containsKey("uuid") ||
                    resourceHashMap.containsKey("_id") || resourceHashMap.containsKey("id")) {
                // Grab the specified ID
                String incomingUUIDString = null;
                if (resourceHashMap.containsKey("UUID")) {
                    incomingUUIDString = resourceHashMap.get("UUID").toString();
                }
                if (incomingUUIDString == null && resourceHashMap.containsKey("uuid")) {
                    incomingUUIDString = resourceHashMap.get("uuid").toString();
                }
                if (incomingUUIDString == null && resourceHashMap.containsKey("_id")) {
                    incomingUUIDString = resourceHashMap.get("_id").toString();
                }
                if (incomingUUIDString == null && resourceHashMap.containsKey("id")) {
                    incomingUUIDString = resourceHashMap.get("id").toString();
                }

                // Now make sure it's actually a valid UUID
                if (incomingUUIDString != null) {
                    try {
                        incomingUUID = UUID.fromString(incomingUUIDString);
                    } catch (Exception e) {
                        logger.warning("The UUID of the incoming resource was not valid " + incomingUUIDString +
                                ":" + e.getMessage());
                    }

                    // If it's valid, search the data store for one
                    if (incomingUUID != null) {
                        String existingResource = dataStore.getResourceByUUID(incomingUUID);
                        if (existingResource != null) {
                            resourceExists = true;
                        }
                    }
                }
            }

            // See if a matching resource was found
            if (resourceExists) {
                // Return an conflict
                return Response.status(Response.Status.CONFLICT).build();
            } else {
                // Since it was not, we need to see if there is one with a matching locator.  Check to see if the
                // locators were specified
                if (resourceHashMap.containsKey("locators")) {
                    // Grab the locators object
                    Object locatorsObject = resourceHashMap.get("locators");

                    // Make sure it's an array list, which is what we expect
                    if (locatorsObject != null && locatorsObject instanceof ArrayList) {
                        // Grab the array
                        ArrayList<String> locators = (ArrayList<String>) resourceHashMap.get("locators");

                        // Loop over them
                        for (String locator : locators) {
                            // Search by locator
                            String jsonResult = dataStore.getResourceByLocator(locator);
                            if (jsonResult != null) {
                                resourceExists = true;
                                break;
                            }
                        }
                    }
                }

                // Check to see if the resource was found by locators
                if (resourceExists) {
                    // Return an conflict
                    return Response.status(Response.Status.CONFLICT).build();
                } else {
                    // Since no matching resource was found, we need to index a new one. If no UUID was specified coming
                    // in, we need to create a new one.
                    if (incomingUUID == null) {
                        incomingUUID = UUID.randomUUID();

                        // Add it to the JSON string
                        if (!resourceHashMap.containsKey("uuid")) {
                            resourceHashMap.put("uuid", incomingUUID.toString());
                        }
                    }

                    // We should have all we need, let's index it
                    String jsonResource = gson.toJson(resourceHashMap, LinkedHashMap.class);
                    logger.fine("Incoming resource to create: " + jsonResource);

                    // Now use the data store to index it
                    String id = dataStore.indexResource(incomingUUID, jsonResource);
                    logger.fine("ID from created Resource is " + id);

                    // Now generate the response
                    URI createdURI = uriInfo.getBaseUriBuilder().path(id).build();

                    // And send it
                    return Response.created(createdURI).build();
                }
            }
        } else {
            // Generate an unauthorized response
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

}
