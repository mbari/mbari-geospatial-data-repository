# MBARI's Geospatial Data Repository Project

This project is designed as a file repository that is geospatially aware and will provide additional functionality and services based on content type. This is done using a set of coordinated servers that are all providing various services.  To get up and running quickly, Docker is used (with docker compose).

## Up and running

1. Create SSL certificate. In order to provide SSL for the repository server an SSL certificate and key must be generated before docker compose is running. Normally for production, you would get this certificate from a CA, but for development, you can run the following to generate them.
    1. ```
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./components/httpd/localhost.key -out ./components/httpd/localhost.crt
    ```
1. Install Docker (make sure Docker Compose is installed too)
1. Copy the docker-compose.yml.template file to docker-compose.yml and edit
1. Copy .env.template to .env and edit (see comments in file)
1. Make sure your data repository directory has the following structure:
    - data (directory where the data that will be monitored will go)
    - elasticsearch/data
    - elasticsearch/logs
    - fswatch/logs
    - httpd/logs
    - kibana/data
    - kibana/logs
    - rabbitmq/logs
    - rabbitmq/mnesia
1. Copy components/httpd/auth_openidc.conf.template to auth_openidc.conf and edit with your authentication provider information. See https://github.com/pingidentity/mod_auth_openidc and https://github.com/pingidentity/mod_auth_openidc/blob/master/auth_openidc.conf for more information
1. Run 'docker-compose up' (this will take a LONG time the first time as it has to build the various images)

Once the system is up and running, you should be able to bring up a directory listing of the files in the repository by visiting https://localhost/data.  You can get to the RabbitMQ web console by visiting https://localhost/rabbitmq-mgmt and using the username and password that are defined in the docker-compose.yml file.  You can get to the Kibana console by visitig https://localhost/kibana and using the username and password defined in the components/kibana/kibana.yml file.

# Components
## Environment File
There is a template named .env.template that should be used to create an environment file for the docker containers to use.  The file should be named .env and points the docker containers to the location where all the data and logs will be written.

## RabbitMQ
There was a need in the repository for a messaging system that other parts of the repository could subscribe and listen to.  The idea is that when a file changes in the repository, there are several services that care about that change, not to mention other applications that may care also.  A RabbitMQ container provides this functionality.  Since it is a foundational piece of the repository, it is started first inside the docker compose file.  It does not have any dependencies and uses a stock docker image to run.  There is an init.sh script that is used to start the Rabbitmq service inside the container and makes sure that all the proper users (admin and client), vhosts, and permissions are set.  In the data repository location set up in the .env file, there will be a rabbitmq directory which contains the data for the server itself as well as the log files from the server.  NOTE: As of this writing, even though the volume mounts for the RabbitMQ container should put the logs for the server in the log directory, for some unknown reason, that is not working as far as I can tell.

The RabbitMQ server exposes ports 5672 and 15672 only to other containers in the docker compose.  5672 is the message port and 15672 is the web console which is then proxied by the httpd component that is later in the docker compose file.

## FSWATCH
The fswatch component is built from a Dockerfile contained in components/fswatch directory.  It uses a base CentOS 7 image and then uses 4 projects to build the functionality to monitor a file directory.  First, it checks out (using git) the protocol buffers project from https://github.com/google/protobuf.  It builds the protocol buffer compiler so that it can be used later to build another project.  The second project is the FSWATCH project located here:

https://github.com/emcrisostomo/fswatch

The docker build downloads the source from this project and builds it into the image as an executable that will be used by a shell script.  Next, the docker build checks out the core model classes from the project hosted here:

https://bitbucket.org/mbari/data-tag-modeling/src/master/

and runs a maven clean, package and install to install the appropriate compiled library in the maven repository and make the classes available to the fswatch server which is built next.  It then checks out the code from this repository:

https://bitbucket.org/mbari/file-to-es-indexer/src/master/

and runs a maven clean, compile, assembly:single to generate the jar file that will be used to run the fswatch server.  Once the build is complete, the CMD is set so that when the container starts up it runs a shell script which does two things.  It first starts a Java process that is a socket server that listens for messages on a specific port.  It then starts the fswatch executable which monitors the directory specified in the .env file and sends fswatch messages to the Java server when something changes in the directory structure.  These messages are translated by the Java server and then published to the RabbitMQ server where they are then picked up by other processes that have subscribed to those exchanges.

## Elasticsearch
This service is pretty much a stock instance of an Elasticsearch server, but I do override a couple of things like the location of the log and data files.  In the docker compose, I expose ports 9200 and 9300 to other containers orchestrated with this compose file.  The compose file has an entry to open those same ports to the outside, but you should only really need those if you are developing or debugging, they should be commented out for production.

## Kibana
This is the visual front end for the elasticsearch server and is pretty much a stock install of Kibana.  I expose port 5601 to the other containers, but not to the outside world by default as the port should be proxied through the httpd serivce (see below).  If you want to access Kibana directly, you need to uncomment out the "ports" section of the kibana service in the docker-compose.yml file, and also comment out the "server.basePath" property in the components/kibana/kibana.yml file.  The server base path is necessary when proxying through the httpd service.  The kibana.yml file also overrides the default data and log locations.

## HTTPD
This component is a little complicated because it serves many functions.  The most basic is that it serves to proxy the various HTTP services that are exposed by other containers.  These ports are:
1. RabbitMQ's port 15672 (web console) is proxied to the URL path /rabbitmq-mgmt
1. Kibana's port 5601 is proxied to the URL path /kibana
It also provides the authentication proxy using Auth0 as the OpenID provider.  This means that the HTTPD server deals with authenticating the user through Auth0, and then provides each of the proxied services, the authenticated user information.  There is a Dockerfile that is used to build this custom HTTPD server and make sure all the proper libraries and modules are built and installed to support this functionality.  There is a auth_openidc.conf.template file that must be copied and edited with your Auth0 information before the Docker image is built.  The auth_openidc.conf file is added to the image during the build to configure the functionality of the OpenID service.  As listed in the introduction at the top of this doc, you also need to have HTTPS certificates so that they can be used to provide HTTPS.  There is a custom run-httpd.sh file that needed to be used to get the container to properly shutdown and startup (I found this on the web somewhere).
