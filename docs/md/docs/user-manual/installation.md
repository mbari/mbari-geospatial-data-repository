# Installation Instructions

To build a geospatially aware data repository, there are many, many moving parts.  As time goes on, we will be working to simplify these installation instructions, but this complexity is due to the nature of this project which is an integration of many technologies to provide the functionality of a geospatially aware data repository.

## Installing on CentOS 7 (Virtual Machine)

1. Install CentOS
    1. Go to the CentOS [download website](https://www.centos.org/download/) and download the "Everything ISO"
    1. Start up your VM Hosting environment (VMWare Fusion, etc.)
    1. Install CentOS 7 using the downloaded ISO (instructions not detailed here).  A couple small notes, I had to actually turn on the network adapter in CentOS and then did a software update for everything before moving on.
1. Get this project
    1. Install git using ```sudo yum install git```
    1. Then go to a directory in your home directory and run ```git clone https://kgomes@bitbucket.org/mbari/mbari-geospatial-data-repository.git```.  This will download this project into your home directory.
1. Install Apache
    1. Got to Applications > System Tools > Software
    1. In the search box, type 'httpd' and hit Enter
    1. You should see a list that includes Apache HTTP Server (2.4.6-45 at the time of this writing).
    1. Check the box next to that entry and click on "Apply Changes"
    1. NOTE: you can probably just use ```sudo yum install httpd``` instead of the above steps.
    1. Open a terminal window and use the following commands to open holes in the local firewall for ports 80 and 443
    ```
    sudo firewall-cmd --permanent --add-port=80/tcp
    sudo firewall-cmd --permanent --add-port=443/tcp
    ```
    1. The reload the firewall using:
    ```
    sudo firewall-cmd --reload
    ```
    1. Then start the apache server using
    ```
    sudo systemctl start httpd
    ```
    1. Set it to start on boot
    ```
    sudo systemctl enable httpd
    ```
    1. Open firefox and try to browse to http://localhost
1. Set up https
    1. Usually, the default Apache server does not come with mod_ssl installed.  If it does not, install it using:
    ```
    sudo yum install mod_ssl
    ```
    1. Restart httpd using ```sudo systemctl restart httpd```.  Once the restart is done, you can browse to https://localhost.  This will give you a security exception because the certificate used in the base SSL install is not a real one, but you can add a security exception, just to make sure you can see the default web page over https. There will also be new log files in the /var/log/httpd directory that apply to just the SSL server.
    1. Now I want to create a local server that is aliases to data-repo.mbari.org for development purposes, so I need to create a new VirtualHost in apache that is using HTTPS.  In order to do that, I first need to add an entry in the /etc/hosts file so the localhost can resolve that name.  I edited /etc/hosts and added the following line:
    ```
    127.0.0.1    data-repo.mbari.org
    ```
    After adding this line, you should be able to run ```ping data-repo.mbari.org``` from the command line and get a ping response from 127.0.0.1.
    1. Next, to give the virtual host a place to server files from, I created a new directory using:
    ```
    sudo mkdir /var/www/html/data-repo.mbari.org
    ```
    And then changed ownership of the directory to my user ```sudo chown kgomes:kgomes /var/www/html/data-repo.mbari.org``` so I could write stuff to it.  Also, make sure the directory is readable by the world so the apache process can read the files in it.
    1. I then created a skeleton html file in the data-repo.mbari.org directory with the following (just for testing):
    ```
    <html>
      <head>
        <title>Test page for data-repo.mbari.org</title>
      </head>
      <body>
        <h1>Test page for data-repo.mbari.org</h1>
      </body>
    </html>
    ```
    You should be able to browse to http://data-repo.mbari.org/data-repo.mbari.org or https://data-repo.mbari.org/data-repo.mbari.org (after adding another exception) to see the file you just created.
    1. For the purposes of this development installation, I am going to use a self-signed certificate for the data-repo.mbari.org virtual host, but you, obviously, should use a real certificate from a CA when you do this in production.
    1. I first created a 'ssl' directory in the /etc/httpd directory using:
    ```
    cd /etc/httpd
    sudo mkdir ssl
    ```
    1. I then created a self-signed certificate using:
    ```
    sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/mbari-data-repository.key -out /etc/httpd/ssl/mbari-data-repository.crt
    ```
    You will be prompted for appropriate information for the certificate. For these instructions, I used the hostname data-repo.mbari.org, which I will resolve locally to 127.0.0.1 for the purposes of this exercise.
    1. Now edit the /etc/httpd/conf.d/ssl.conf file (```sudo vi /etc/httpd/conf.d/ssl.conf``` works).  There is already a VirtualHost entry for the base directory, so we need to add a new one.
    1. I added the following to the bottom of the file:
    ```
    <VirtualHost *:443>
    DocumentRoot "/var/www/html/data-repo.mbari.org"
    ServerName data-repo.mbari.org:443
    ErrorLog "/var/log/httpd/data-repo.mbari.org-error_log"
    TransferLog "/var/log/httpd/data-repo.mbari.org-ssl-access_log"
    SSLEngine on
    SSLCertificateFile "/etc/httpd/ssl/mbari-data-repository.crt"
    SSLCertificateKeyFile "/etc/httpd/ssl/mbari-data-repository.key"
    </VirtualHost>
    ```
    And the restarted the httpd server.  You should then be able to browse to https://data-repo.mbari.org (after adding yet another exception) and you should see your startup test HTML file over SSL.
1. Install mod_auth_openidc
    1. After getting SSL up and running on our virtual host, we now want to protect the content on the site so that anyone who wants to access it must log in.  There is a module called mod_auth_openidc that can be installed that will allow us to use the Apache http server as a gateway to protect resources using OpenID Connect.  So let's install that first using:
    ```
    sudo yum install mod_auth_openidc
    ```
    This installs the apache module and also creates a configuration file template, /etc/conf.d/auth_openidc.conf.  All of the options are commented out of the configuration file for now.  I would recommend making a backup of this file because these configurations can be a bit tricky.  I created a copy in my home directory under a directory named data-repo.mbari.org for safekeeping in case I need it later.
    1. Now in order to get OpenID Connect working, you need an OpenID Provider and for this exercise, I used Auth0 (https://auth0.com).  These instructions will be for this provider and will be different if you use a different provider.
    1. After logging in to the Auth0 site, I clicked on "Clients" on the left hand menu, then clicked on the orange "+ Create Client" button.
    1. I gave it a name of "MBARI Data Repository Dev" and chose a client type of "Non Interactive Clients", then clicked on "Create".
    1. Once the client was created and the dashboard came up, I clicked on "Settings".
    1. I scrolled down and added a "Allowed Callback URL" of https://data-repo.mbari.org/redirect_uri.  While this URL doesn't exist specifically, it can be used to redirect the client back to the site after logging in. I then clicked on Save Changes
    1. Then scroll back down to the bottom and click on "Advanced Settings" and then on "Endpoints" as you will need this information when you configure mod_auth_openidc.
    1. Now edit the /etc/httpd/conf.d/auth_openidc.conf file and uncomment/edit or add the information from your Auth0 client endpoints.  My configuration looked like this:
    ```
    OIDCRedirectURI https://data-repo.mbari.org/redirect_uri
    OIDCCryptoPassphrase xxxxxxxxxxxxxxxx
    OIDCProviderMetadataURL https://mbari.auth0.com/.well-known/openid-configuration
    OIDCProviderIssuer https://mbari.auth0.com
    OIDCProviderAuthorizationEndpoint https://mbari.auth0.com/authorize
    OIDCProviderJwksUri https://mbari.auth0.com/.well-known/jwks.json
    OIDCProviderTokenEndpoint https://mbari.auth0.com/oauth/token
    OIDCProviderTokenEndpointAuth client_secret_post
    OIDCProviderUserInfoEndpoint https://mbari.auth0.com/userinfo
    OIDCScope "openid email profile"
    OIDCClientID xxxxxxxxxxxxxxxx
    OIDCClientSecret xxxxxxxxxxxxxxxx
    OIDCCookiePath /
    ```
    Obviously, I did not want to include my values for Passphrase, ClientID and ClientSecret, but that is where they go.
    1. The last step is to tell the Apache server, which location is to be protected by the OpenID Connect authenticator.  Back in the /etc/httpd/conf.d/ssl.conf file, the following section should be added *inside* the VirtualHost entry created earlier:
    ```
    <Location />
       AuthType openid-connect
       Require valid-user
       LogLevel debug
    </Location>
    ```
    So the final VirtualHost will look like:
    ```
    <VirtualHost *:443>
    DocumentRoot "/var/www/html/data-repo.mbari.org"
    ServerName data-repo.mbari.org:443
    ErrorLog "/var/log/httpd/data-repo.mbari.org-error_log"
    TransferLog "/var/log/httpd/data-repo.mbari.org-ssl-access_log"
    SSLEngine on
    SSLCertificateFile "/etc/httpd/ssl/mbari-data-repository.crt"
    SSLCertificateKeyFile "/etc/httpd/ssl/mbari-data-repository.key"
    <Location />
       AuthType openid-connect
       Require valid-user
       LogLevel debug
    </Location>
    </VirtualHost>
    ```
    Then restart the apache server.  If you now browse to https://data-repo.mbari.org, you will be redirected to your OpenID Connect provider site and then when you login, you will be redirected back to your test page.  In my case, I used my Google login (which is set up for two-factor auth) and then was redirected back to my test page.
    1. So after all that, not much to see huh? Actually, you have done quite a bit.  If you are interested in seeing more of what is going on, you can do the following:
      1. Install PHP using ```sudo yum install php```
      1. Restart the httpd service
      1. Then create a file in the /var/www/html/data-repo.mbari.org directory named php-test.php and enter the following:
      ```
      <html>
        <head>
            <title>Header Dump</title>
        </head>
        <body>
          <h1>Here are the request headers that came in on this page request</h1>
              Remote User: <?php echo($_SERVER['REMOTE_USER']) ?>
          <pre><?php print_r(array_map("htmlentities", apache_request_headers())); ?></pre>
        </body>
      </html>
      ```
      1. You can then browse to: https://data-repo.mbari.org/php-test.php.  This will take you back out to your OpenID Connect provider (because Apache was restarted, it does not remember your last session) and then once you authenticate you should see something like:
      ```
      Here are the request headers that came in on this page request
      Remote User: google-oauth2|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      Array
      (
          [Host] => data-repo.mbari.org
          [User-Agent] => Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101   Firefox/52.0
          [Accept] => text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
          [Accept-Language] => en-US,en;q=0.5
          [Accept-Encoding] => gzip, deflate, br
          [Referer] => https://mbari.auth0.com/login?client=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&protocol=oauth2&redirect_uri=https%3A%2F%2Fdata-repo.mbari.org%2Fredirect_uri&response_type=code&scope=openid%20email%20profile&nonce=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&state=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          [Cookie] => mod_auth_openidc_session=5ff78039-6d81-4a2a-912b-d6e0564a84a4
          [Connection] => keep-alive
          [Upgrade-Insecure-Requests] => 1
          [OIDC_CLAIM_sub] => google-oauth2|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          [OIDC_CLAIM_email] => xxxxxxxxxxxxxx
          [OIDC_CLAIM_locale] => en
          [OIDC_CLAIM_email_verified] => 1
          [OIDC_CLAIM_name] => Kevin Gomes
          [OIDC_CLAIM_family_name] => Gomes
          [OIDC_CLAIM_given_name] => Kevin
          [OIDC_CLAIM_nickname] => kgomes
          [OIDC_CLAIM_picture] =>  https://lh6.googleusercontent.com/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/photo.jpg
          [OIDC_CLAIM_gender] => male
          [OIDC_CLAIM_updated_at] => 2017-09-08T19:45:38.126Z
          [OIDC_CLAIM_iat] => 1504899938
          [OIDC_CLAIM_iss] => https://mbari.auth0.com/
          [OIDC_CLAIM_aud] => xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          [OIDC_CLAIM_exp] => 1504935938
          [OIDC_CLAIM_nonce] => xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          [OIDC_access_token] => xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          [OIDC_access_token_expires] => 1504986339
      )
      ```
      I replaced any potential security information with 'xxxxxxxxx...' above, but you get the idea.  What you're seeing in these headers is the information that was returned from your OpenID Connect Provider and some of the important ones for this software are the OIDC_CLAIM_sub, OIDC_CLAIM_email, and the OIDC_access_token.  These are basically unique identifiers telling the receiver of the request who the requester is.  Now that we have Apache managing authentication using OpenID Connect, we can then allow that information to flow to backend services so that we are basically providing Single-Sign-On for these applications.
1. Install Tomcat that will be used with ERDDAP
  1. The first of these applications for the data repository is ERDDAP.  I started with this application because it was the most difficult to integrate into this repository scheme.  In short, in order to allow ERDDAP to provide role-based authorization, authentication must be enabled.  But the way the code is written, the Tomcat server must be running over https in order to recognize any kind of logins.  This is a problem when you want to run the SSL on the Apache reverse proxy in front of ERDDAP.  So you basically have to run through two SSL negotiations for every request.  ICK!  It is what it is and until something changes, this was how I got it to work.
  1. The first thing to do is make sure the Oracle Java is installed.  I have found the OpenJDK one that comes with Centos to be somewhat lacking.  To do this browse to http://www.oracle.com/technetwork/java/javase/downloads/index.html and download the JRE and the JDK, both in rpm format. Install both of them using ```sudo yum localinstall (jdk/jre..).rpm```.
  1. I had to switch the default java to use the newly installed java using ```sudo alternatives --config java``` and choosing the new JRE install.
  1. Download the latest Tomcat server from http://tomcat.apache.org.  I downloaded the tgz format.
  1. In order to run tomcat safely, you will want to run it as a different user with limited permissions.  So create a new tomcat group using ```sudo groupadd tomcat``` and then a new user using ```sudo useradd -M -s /bin/nologin -g tomcat -d /home/tomcat tomcat```
  1. Your user account will need access to the various parts of the tomcat installation, so add your user to the tomcat group using ```sudo usermod -a -G tomcat yourusername``` Note that you have to logout and log back in for group adds to take affect.
  1. Create a installation directory using ```sudo mkdir /opt/tomcat-erddap``` and then extract the Tomcat installation using ```sudo tar xvf apache-tomcat-9.0.0.M26.tar.gz -C /opt/tomcat-erddap --strip-components=1```
  1. Now the tomcat install is all owned by root so change that to the Tomcat user by running ```sudo chgrp -R tomcat /opt/tomcat-erddap```
  1. Now give tomcat group read access to the conf directory and all of its contents and execute access to the directory itself using ```sudo chmod -R g+r conf``` and ```sudo chmod g+x conf```
  1. Next make tomcat user the owner of the webapps, work, temp and logs directories using ```sudo chown -R tomcat webapps/ work/ temp/ logs/```
  1. Now edit a systemd unit file by running ```sudo vi /etc/systemd/system/tomcat-erddap.service``` and paste in:

  ```
  # Systemd unit file for tomcat
  [Unit]
  Description=ERDDAP Apache Tomcat Web Application Container
  After=syslog.target network.target

  [Service]
  Type=forking

  Environment=JAVA_HOME=/usr/java/default
  Environment=CATALINA_PID=/opt/tomcat-erddap/temp/tomcat.pid
  Environment=CATALINA_HOME=/opt/tomcat-erddap
  Environment=CATALINA_BASE=/opt/tomcat-erddap
  Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
  Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

  ExecStart=/opt/tomcat-erddap/bin/startup.sh
  ExecStop=/bin/kill -15 $MAINPID

  User=tomcat
  Group=tomcat
  UMask=0007
  RestartSec=10
  Restart=always

  [Install]
  WantedBy=multi-user.target
  ```
  1. Now tell the system to load the unit file using ```sudo systemctl daemon-reload```
  1. Now you can start tomcat using ```sudo systemctl start tomcat-erddap```
  1. If you take a look at the files in /opt/tomcat-erddap/logs there should be one called catalina.out that has the final line that states the server started up on XXXX ms.  That means it's running and you should be able to navigate to http://localhost:8080 to get the tomcat welcome page.
  1. To enable tomcat to run at boot, use ```sudo systemctl enable tomcat-erddap```
1. Configure ERDDAP Tomcat for https
  1. Now that we have Tomcat running and responding over http on port 8080, we need to set it up to run over https.  I first created a place where I can put the Java keystore that will be needed for this.  I did that by running ```sudo mkdir /opt/tomcat-erddap/ssl```.
  1. I then changed the ownership to tomcat using ```sudo chown tomcat:tomcat /opt/tomcat-erddap/ssl```
  1. I then created the keystore using ```sudo keytool -genkey -alias tomcat-erddap -keyalg RSA -keystore /opt/tomcat-erddap/ssl/erddap.keystore```. I had to enter all the appropriate information at the prompts and you will have to do the same.  Remember the both the key and the keystore passwords you created. *Important Note*: To make things easy, use the same password for the keystore as the tomcat key.
  1. Now I made sure tomcat was the owner of the keystore by running ```sudo chown tomcat:tomcat /opt/tomcat-erddap/ssl/erddap.keystore```
  1. I then edited the server.xml file using ```sudo vi /opt/tomcat-erddap/conf/server.xml``` and added the following just after the port 8080 Connector section:
  ```
  <!-- This is a connector to open port 8443 for SSL communications -->
  <Connector
           protocol="org.apache.coyote.http11.Http11NioProtocol"
           port="8443" maxThreads="200"
           scheme="https" secure="true" SSLEnabled="true"
           keystoreFile="/opt/tomcat-erddap/ssl/erddap.keystore" keystorePass="xxxxxxx"
           clientAuth="false" sslProtocol="TLS"/>
  ```
  1. Now restart tomcat using ```sudo systemctl restart tomcat-erddap```
  1. You should then be able to browser to https://data-repo.mbari.org:8443 and (after adding a security exception) you should see the Tomcat home page served over https.
1. Install ERDDAP
  1. The next step is to actually install the ERDDAP application which is a web application WAR file that is installed inside Tomcat.  However, there is some configuration that needs to be done prior to installing the WAR file.
  1. It is highly recommended that you install the BitstreamVeraSans fonts in Tomcat first.  Download those fonts [here](https://coastwatch.pfeg.noaa.gov/erddap/download/BitstreamVeraSans.zip).
  1. Once the file is downloaded, place it in a temporary directory and unzip it.  You will find a handful of .ttf files.
  1. Move all the .ttf files to the JAVA_HOME/lib/fonts directory so they are available to any Java application. I also placed them in the JAVA_HOME/jre/lib/fonts directory.
  1. ERDDAP has a content zip file all ready to go that you download and configure for your particular installation and this must be done before installing the ERDDAP application.  Download the latest .zip file [here](https://coastwatch.pfeg.noaa.gov/erddap/download/erddapContent.zip).
  1. Unzip that file in the tomcat directory creating the /opt/tomcat-erddap/content/erddap directory. I also ran ```sudo chown -R tomcat:tomcat /opt/tomcat-erddap/content``` to make sure the tomcat process could read the files.
  1. Read the instructions given in the [ERDDAP install instructions](https://coastwatch.pfeg.noaa.gov/erddap/download/setup.html) on how to edit the /opt/tomcat-erddap/content/erddap/setup.xml file and configure to meet your installation. Note that most of what you need are listed in the comments in the setup.xml file.
  1. I tend to create a base /data directory and then use sub directories for each application server.  For this reason, I created /data/erddap, gave ownership to tomcat:tomcat and used that as my *bigParentDirectory* location in the setup.xml file.
  1. Since we are going to be using Apache in front of all of our application servers, we need to do things a little differently in the setup.xml file.  For the <baseUrl> and the <httpsBaseurl> properties, I used ```https://data-repo.mbari.org```.
  1. Before we actually install the ERDDAP application, we need to configure the Apache server as a reverse proxy to the ERDDAP server so the URLs we indicated in the last step work correctly.  With those URLs, ERDDAP is going to be expecting erddap to be located at https://data-repo.mbari.org/erddap so we must configure a reverse proxy to make this happen.
  1. Edit the /etc/httpd/conf.d/ssl.conf file again and add the following section at the bottom of the VirtualHost section you created earlier (but still inside the <VirtualHost> tag):
  ```
  SSLProxyEngine On
  SSLProxyVerify none
  SSLProxyCheckPeerCN off
  SSLProxyCheckPeerName off
  SSLProxyCheckPeerExpire off
  <Location /erddap>
     ProxyPass https://data-repo.mbari.org:8443/erddap
     ProxyPassReverse https://data-repo.mbari.org:8443/erddap
     ProxyPassReverseCookieDomain 127.0.0.1 data-repo.mbari.org
  </Location>
  ```
  1. Then restart the Apache server.  You will not be able to browse to https://data-repo.mbari.org/erddap yet.
  1. Next, download the ERDDAP WAR file from [here](https://github.com/BobSimons/erddap/releases/download/v1.80/erddap.war) Note this is specific to the 1.80 version and you will want to make sure you grab the version that matches the version you downloaded in step where you downloaded the erddapContent.zip file.
  1. Just for consistency, I chown'd the owner of the erddap.war file to tomcat:tomcat.
  1. Then move the erddap.war file into the /opt/tomcat-erddap/webapps directory.  This will actually deploy the application.  If you look at the tail of the /opt/tomcat-erddap/logs/catalina.out file, you should see something like:
  ```
  11-Sep-2017 10:16:47.177 INFO [ContainerBackgroundProcessor[StandardEngine[Catalina]]] org.apache.catalina.startup.HostConfig.deployWAR Deployment of web application archive [/opt/tomcat-erddap/webapps/erddap.war] has finished in [15,130] ms
  ```
  Note that this actually does not start the ERDDAP application indexing quite yet.  If you now browse to https://data-repo.mbari.org/erddap, after a short wait, you should see the ERDDAP home page. WHEW!!!.  If you look at the CPU usage, you will see Tomcat is very busy and if you look at the /data/erddap/logs/log.txt file, you will see ERDDAP is busy indexing various default data sets.  You may notice on the main ERDDAP page that it may say it has 0 data sets.  If you refresh the page after a bit, you will see that number increase as it indexes the default data sets.
  1. As a summary, you now have ERDDAP running and listening over SSL and Apache acting as a reverse proxy to ERDDAP with the addition of now having the users authenticated using OpenID Connect and that user information is actually now being passed to ERDDAP in each of the request headers as they are passed through Apache.  The next step is to actually get ERDDAP to recognize that information as the user identification.  Since the ERDDAP authentication stuff is custom code, I had to create a Tomcat-wide valve to attach the header information in a format the ERDDAP would be looking for.  Basically, ERDDAP checks for a couple of conditions to identify the user.  It makes sure the URLs are coming in over https first.  Then if that is satisfied, it looks for a user session attribute called 'loggedInAs:erddap' to see if a user has logged into the session.  In order to tie this into the OpenID Connect apache proxy, I created a Tomcat valve to map the OpenID header fields into a session variable with that name.
1. Install auth bridge valve in ERDDAP
  1. Install ant using ```sudo yum install ant```.  This is a build tool if you aren't familiar with it.  Yes, it's an old tool, but it does the job (not a fan of maven).
  1. Edit the build.properties file and change the erddap.tomcat.home property to be the full path to the directory where your erddap tomcat installation lives. For these instructions, that would be /opt/tomcat-erddap.
  1. Then at the command line at the top level of the project, run ```ant erddap-auth-valve-jar```.  This will create a erddap-auth-valve.jar file in the dist directory.  Copy that file into the /opt/tomcat-erddap/lib directory.
  1. Edit the /opt/tomcat-erddap/conf/server.xml file and add the following line
  ```
  <Valve className="org.mbari.auth.ErddapAuthValve" />
  ```
  inside the ```<Host name="localhost"  appBase="webapps" unpackWARs="true" autoDeploy="true">``` section before the ending ```</Host>``` tag.
  1. Also, to get logging separated from the rest of the tomcat installation. Open the /opt/tomcat-erddap/conf/logging.properties file and add the following section
  ```
  1FILE.org.apache.juli.FileHandler.level = FINE
  1FILE.org.apache.juli.FileHandler.directory = ${catalina.base}/logs
  1FILE.org.apache.juli.FileHandler.prefix = mbari.
  1FILE.org.apache.juli.FileHandler.maxDays = 90
  org.mbari.level=INFO
  org.mbari.handlers=1FILE.org.apache.juli.FileHandler
  ```
  right before the line:
  ```
  java.util.logging.ConsoleHandler.level = FINE
  ```
  1. Then add the following ```,  1FILE.org.apache.juli.FileHandler``` to the end of the line that starts with ```handlers``` so that it looks like:
  ```
  handlers = 1catalina.org.apache.juli.AsyncFileHandler, 2localhost.org.apache.juli.AsyncFileHandler, 3manager.org.apache.juli.AsyncFileHandler, 4host-manager.org.apache.juli.AsyncFileHandler, java.util.logging.ConsoleHandler, 1FILE.org.apache.juli.FileHandler
  ```
  1. With these changes, each request coming to ERDDAP will have the OIDC email address mapped to the session attribute ```loggedInAs:erddap```, but we have to tell ERDDAP to turn on authentication now.  Edit the /opt/tomcat-erddap/content/erddap/setup.xml file and change the ```<authentication></authentication>``` property to ```<authentication>custom</authentication>```.
  1. Restart the tomcat server using ```sudo systemctl restart tomcat-erddap```
  1. Now browser to https://data-repo.mbari.org/erddap and (you may have to authenticate again) then you should see your authenticated email address in the upper right corner along with a logout link.
1. Install Geoserver
  1. Geoserver is also a web application that will be installed in a very similar manner as ERDDAP.  One thing to note is that Tomcat, by default uses ports 8080 and 8443 for its http and https protocol communication.  Since Geoserver will be installed in it's own instance of Tomcat, we need to change those ports to something different.
1. Configure Apache as https proxy for Geoserver
1. Configure Geoserver authentication using header values pulled from mod_auth_openidc
1. HYRAX??
